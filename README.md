# Overview of compile and runtests

| System         | TEST | compile with CUDA-backend |  compile with HIP-backend | run on NVIDIA | run on AMD GPU |
|----------------|------|---------------------------|---------------------------|---------------|----------------|
| vader (2xA100) | device Info CUDA | yes | yes | yes | yes, but no info | 
| lumi (4xMI100) | device Info HIP  | no  | yes | -   | yes |
