/*-----------------------------------------------------------
  taken from open source rocm repo
  git@github.com:ROCm-Developer-Tools/HIP-Examples.git

LICENSE: https://github.com/ROCm-Developer-Tools/HIP-Examples/blob/master/rodinia_3.0/LICENSE
Copyright (c)2008-2014 University of Virginia

build with
nvcc print.cu 


 ** original: gaussian.cu -- The program is to solve a linear system Ax = b
 **
 ** Written by Andreas Kura, 02/15/95
 ** Modified by Chong-wei Xu, 04/20/95
 ** Modified by Chris Gregg for CUDA, 07/20/2009
 **-----------------------------------------------------------
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "cuda.h"
#include <string.h>
#include <math.h>

#ifdef RD_WG_SIZE_0_0
#define MAXBLOCKSIZE RD_WG_SIZE_0_0
#elif defined(RD_WG_SIZE_0)
#define MAXBLOCKSIZE RD_WG_SIZE_0
#elif defined(RD_WG_SIZE)
#define MAXBLOCKSIZE RD_WG_SIZE
#else
#define MAXBLOCKSIZE 512
#endif

//2D defines. Go from specific to general                                                
#ifdef RD_WG_SIZE_1_0
#define BLOCK_SIZE_XY RD_WG_SIZE_1_0
#elif defined(RD_WG_SIZE_1)
#define BLOCK_SIZE_XY RD_WG_SIZE_1
#elif defined(RD_WG_SIZE)
#define BLOCK_SIZE_XY RD_WG_SIZE
#else
#define BLOCK_SIZE_XY 4
#endif

void PrintDeviceProperties();

int main(int argc, char *argv[])
{
  PrintDeviceProperties();

}
/*------------------------------------------------------
 ** PrintDeviceProperties
 **-----------------------------------------------------
 */
void PrintDeviceProperties(){
  cudaDeviceProp deviceProp;  
  int nDevCount = 0;  

  cudaGetDeviceCount( &nDevCount );  
  printf( "Total Device found: %d", nDevCount );  
  for (int nDeviceIdx = 0; nDeviceIdx < nDevCount; ++nDeviceIdx )  
  {  
    memset( &deviceProp, 0, sizeof(deviceProp));  
    if( cudaSuccess == cudaGetDeviceProperties(&deviceProp, nDeviceIdx))  
    {
      printf( "\nDevice Name \t\t - %s ", deviceProp.name );  
      printf( "\n**************************************");  
      printf( "\nTotal Global Memory\t\t\t - %lu KB", deviceProp.totalGlobalMem/1024 );  
      printf( "\nShared memory available per block \t - %lu KB", deviceProp.sharedMemPerBlock/1024 );  
      printf( "\nNumber of registers per thread block \t - %d", deviceProp.regsPerBlock );  
      printf( "\nWarp size in threads \t\t\t - %d", deviceProp.warpSize );  
      printf( "\nMemory Pitch \t\t\t\t - %zu bytes", deviceProp.memPitch );  
      printf( "\nMaximum threads per block \t\t - %d", deviceProp.maxThreadsPerBlock );  
      printf( "\nMaximum Thread Dimension (block) \t - %d %d %d", deviceProp.maxThreadsDim[0], deviceProp.maxThreadsDim[1], deviceProp.maxThreadsDim[2] );  
      printf( "\nMaximum Thread Dimension (grid) \t - %d %d %d", deviceProp.maxGridSize[0], deviceProp.maxGridSize[1], deviceProp.maxGridSize[2] );  
      printf( "\nTotal constant memory \t\t\t - %zu bytes", deviceProp.totalConstMem );  
      printf( "\nCUDA ver \t\t\t\t - %d.%d", deviceProp.major, deviceProp.minor );  
      printf( "\nClock rate \t\t\t\t - %d KHz", deviceProp.clockRate );  
      printf( "\nTexture Alignment \t\t\t - %zu bytes", deviceProp.textureAlignment );  
      printf( "\nDevice Overlap \t\t\t\t - %s", deviceProp. deviceOverlap?"Allowed":"Not Allowed" );  
      printf( "\nNumber of Multi processors \t\t - %d\n\n", deviceProp.multiProcessorCount );  
    }  
    else  
      printf( "\n%s", cudaGetErrorString(cudaGetLastError()));  
  }  
}
